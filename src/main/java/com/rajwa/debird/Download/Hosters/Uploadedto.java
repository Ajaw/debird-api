package com.rajwa.debird.Download.Hosters;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.util.JSON;
import com.rajwa.debird.Download.Client;
import com.rajwa.debird.Download.DownloadLink;
import com.rajwa.debird.Download.Encoding;
import com.rajwa.debird.Download.Regex;
import com.rajwa.debird.Model.JD.Account;
import com.rajwa.debird.Model.JD.AccountInfo;
import okhttp3.FormBody;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.BufferedSink;
import okio.BufferedSource;
import okio.Okio;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.*;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Pattern;

/**
 * Created by Dominik on 08.07.2018.
 */
@Service
public class Uploadedto {

    private Logger logger = LoggerFactory.getLogger(Uploadedto.class);

    // DEV NOTES:
    // other: respects https in download methods, even though final download
    // link isn't https (free tested).
    /* Enable/disable usage of multiple free accounts at the same time */
    private static final boolean ACCOUNT_FREE_CONCURRENT_USAGE_POSSIBLE = true;
    private static final boolean ACCOUNT_PREMIUM_CONCURRENT_USAGE_POSSIBLE = true;
    private static final int ACCOUNT_FREE_MAXDOWNLOADS = 1;
    /* note: CAN NOT be negative or zero! (ie. -1 or 0) Otherwise math sections fail. .:. use [1-20] */
    private static AtomicInteger totalMaxSimultanFreeDownload = new AtomicInteger(20);
    /* don't touch the following! */
    private static AtomicInteger maxFree = new AtomicInteger(1);
    private static final int ACCOUNT_PREMIUM_MAXDOWNLOADS = -1;
    private static AtomicInteger maxPrem = new AtomicInteger(1);
    // spaces will be '_'(checkLinks) and ' '(requestFileInformation), '_' stay '_'
    private char[] FILENAMEREPLACES = new char[]{' ', '_', '[', ']'};
    /* Reconnect-workaround-related */
    private static final long FREE_RECONNECTWAIT = 3 * 60 * 60 * 1000L;
    private String PROPERTY_LASTIP = "UPLOADEDNET_PROPERTY_LASTIP";
    private static final String PROPERTY_LASTDOWNLOAD = "uploadednet_lastdownload_timestamp";
    private final String ACTIVATEACCOUNTERRORHANDLING = "ACTIVATEACCOUNTERRORHANDLING";
    private final String EXPERIMENTALHANDLING = "EXPERIMENTALHANDLING";
    private Pattern IPREGEX = Pattern.compile("(([1-2])?([0-9])?([0-9])\\.([1-2])?([0-9])?([0-9])\\.([1-2])?([0-9])?([0-9])\\.([1-2])?([0-9])?([0-9]))", Pattern.CASE_INSENSITIVE);
    private static AtomicReference<String> lastIP = new AtomicReference<String>();
    private static AtomicReference<String> currentIP = new AtomicReference<String>();
    private static HashMap<String, Long> blockedIPsMap = new HashMap<String, Long>();
    private static Object CTRLLOCK = new Object();
    private static String[] IPCHECK = new String[]{"http://ipcheck0.jdownloader.org", "http://ipcheck1.jdownloader.org", "http://ipcheck2.jdownloader.org", "http://ipcheck3.jdownloader.org"};
    private static AtomicBoolean usePremiumAPI = new AtomicBoolean(true);
    private static final String NOCHUNKS = "NOCHUNKS";
    private static final String NORESUME = "NORESUME";
    private static final String SSL_CONNECTION = "SSL_CONNECTION";
    private static final String PREFER_PREMIUM_DOWNLOAD_API = "PREFER_PREMIUM_DOWNLOAD_API_V2";
    private static final String DOWNLOAD_ABUSED = "DOWNLOAD_ABUSED";
    private static final String DISABLE_START_INTERVAL = "DISABLE_START_INTERVAL";
    private static final String EXPERIMENTAL_MULTIFREE = "EXPERIMENTAL_MULTIFREE2";
    private boolean PREFERSSL = true;
    private boolean avoidHTTPS = false;
    private static final String UPLOADED_FINAL_FILENAME = "UPLOADED_FINAL_FILENAME";
    private static final String CURRENT_DOMAIN = "http://uploaded.net/";
    private static final String HTML_MAINTENANCE = ">uploaded\\.net - Maintenance|Dear User, Uploaded is in maintenance mode|Lieber Kunde, wir führen kurze Wartungsarbeiten durch";


    private String getProtocol() {
        return "https://";
    }

    public AccountInfo apiFetchAccountInfo(Account account) {
        AccountInfo ai = new AccountInfo();
        try {
            String token = api_getAccessToken(account, false);
            // String token = "AR6n82IN25QN6bViJHtZ";
            String tokenType = null;
            try {
                tokenType = api_getTokenType(account, token, true);
            } catch (final Exception e) {
                token = api_getAccessToken(account, false);
                tokenType = api_getTokenType(account, token, true);
            }
            if ("free".equals(tokenType)) {
                account.setValid(true);
                /* free user */
                ai.setUnlimitedTraffic();
                ai.setValidUntil(-1);
                ai.setStatus("Free account");
                account.setProperty("free", true);
                account.setType(Account.AccountType.FREE);
            } else if ("premium".equals(tokenType)) {
                //  String traffic = br.getRegex("traffic_left\":\\s*?\"?(\\d+)").getMatch(0);
                long max = 100 * 1024 * 1024 * 1024l;
                long current = Long.parseLong("0");
                ai.setTrafficMax(Math.max(max, current));
                ai.setTrafficLeft(current);
                // String expireDate = br.getRegex("account_premium\":\\s*?\"?(\\d+)").getMatch(0);
//            ai.setValidUntil(Long.parseLong("3213") * 1000);
//            if (current <= 0 || br.containsHTML("download_available\":false")) {
//                final boolean downloadAvailable = br.containsHTML("download_available\":true");
//              //  logger.info("Download_available: " + br.containsHTML("download_available\":true"));
//                String refreshIn = br.getRegex("traffic_reset\":\\s*?(\\d+)").getMatch(0);
//                if (refreshIn != null) {
//                   // throw new AccountUnavailableException("DownloadAvailable:" + downloadAvailable, Long.parseLong(refreshIn) * 1000);
//                } else {
//                    // throw new PluginException(LinkStatus.ERROR_PREMIUM, "DownloadAvailable:" + downloadAvailable, PluginException.VALUE_ID_PREMIUM_TEMP_DISABLE);
//                }
//            }
                ai.setStatus("Premium account");
                account.setProperty("free", false);
                account.setType(Account.AccountType.PREMIUM);
                if (!ai.isExpired()) {
                    account.setValid(true);
                }
            } else {
                //throw new PluginException(LinkStatus.ERROR_PLUGIN_DEFECT);
            }

        } catch (final Exception e) {

        }

        return ai;
    }


    private String apiAccesToken;

    private String api_getAccessToken(Account account, boolean liveToken) throws Exception {
        if (this.apiAccesToken == null) {
            try {
                // DANGER: Even after user changed password this token is still valid->Uploaded.to was contacted by psp but no response!
                String token = account.getStringProperty("token", null);
                if (token != null && liveToken == false) {
                    return token;
                }
                /** URLDecoder can make the password invalid or throw an IllegalArgumentException */
                // JDHash.getSHA1(URLDecoder.decode(account.getPass(), "UTF-8").toLowerCase(Locale.ENGLISH))
                String url = getProtocol() + "api.uploaded.net/api/user/login?" + "name=" + Encoding.urlEncode(account.getUser()) + "&pass=" + getLoginSHA1Hash(account.getPass()) + "&ishash=1";
                String urlPost = getProtocol() + "api.uploaded.net/api/user/login";
                RequestBody requestBody = new FormBody.Builder().
                        addEncoded("name", Encoding.urlEncode(account.getUser()))
                        .addEncoded("pass", getLoginSHA1Hash(account.getPass()))
                        .addEncoded("ishash", "1")
                        .addEncoded("app", "JDownloader")
                        .build();
                Client client = new Client();
                //  Response resGet = client.makeGetRequest(url);
                // String  restGet = resGet.body().string();
                Response response = client.makePost(urlPost, requestBody);
                String res = response.body().string();
                JSONObject jsonObject = new JSONObject(res);
                token = jsonObject.getString("access_token");

                //token = br.getRegex("access_token\":\"(.*?)\"").getMatch(0);
//            if (token == null) {
//                //
//                handleErrorCode(br, account, token, true);
//            }
                account.setProperty("token", token);
                this.apiAccesToken = token;
                return token;
            } catch (Exception e) {
                logger.error(e.getMessage());
                e.printStackTrace();
//            if (usePremiumAPI.compareAndSet(true, false)) {
//                getLogger().info("Disable API");
//            }
//            account.setProperty("token", null);
//            account.setProperty("tokenType", null);
//            throw e;
            }
            return null;
        } else {
            return this.apiAccesToken;
        }
    }

    public String getLoginSHA1Hash(String arg) {
        try {
            if (arg != null) {

                arg = arg.replaceAll("(\\\\|\\\"|\0|')", "\\\\$1");
                arg = urlDecode(arg, "ISO-8859-1"); // <<- causes issues with % in pw
                arg = arg.replaceAll("[ \t\n\r\0\u000B]", "");
                while (arg.startsWith("%20")) {
                    arg = arg.substring(3);
                }
                while (arg.endsWith("%20")) {
                    arg = arg.substring(0, arg.length() - 3);
                }
                arg = arg.replaceAll("(\\\\|\\\"|\0|')", "\\\\$1");
                arg = asciiToLower(arg);
                final MessageDigest md = MessageDigest.getInstance("SHA1");
                final byte[] digest = md.digest(arg.getBytes("latin1"));
                return byteArrayToHex(digest);
            }
            return null;
        } catch (final Throwable e) {
            return null;
        }
    }

    private static String urlDecode(String s, String enc) throws UnsupportedEncodingException {
        final int numChars = s.length();
        final StringBuilder sb = new StringBuilder(numChars > 500 ? numChars / 2 : numChars);
        int i = 0;
        if (enc == null || enc.length() == 0) {
            throw new UnsupportedEncodingException("URLDecoder: empty string enc parameter");
        }
        char c;
        byte[] bytes = null;
        loop:
        while (i < numChars) {
            c = s.charAt(i);
            switch (c) {
                case '+':
                    sb.append(' ');
                    i++;
                    break;
                case '%':
                    /*
                     * Starting with this instance of %, process all consecutive substrings of the form %xy. Each substring %xy will yield a
                     * byte. Convert all consecutive bytes obtained this way to whatever character(s) they represent in the provided encoding.
                     */
                    int pos = 0;
                    while (((i + 2) < numChars) && (c == '%')) {
                        final String subString = s.substring(i + 1, i + 3);
                        int v = -1;
                        try {
                            v = Integer.parseInt(subString, 16);
                        } catch (NumberFormatException e) {
                        }
                        if (v < 0) {
                            if (bytes != null && pos > 0) {
                                sb.append(new String(bytes, 0, pos, enc));
                            }
                            sb.append("%");
                            sb.append(subString);
                            i += 3;
                            continue loop;
                        }
                        if (bytes == null) {
                            bytes = new byte[(numChars - i) / 3];
                        }
                        bytes[pos++] = (byte) v;
                        i += 3;
                        if (i < numChars) {
                            c = s.charAt(i);
                        }
                    }
                    // A trailing, incomplete byte encoding such as
                    // "%x" will cause an exception to be thrown
                    if (bytes != null && pos > 0) {
                        sb.append(new String(bytes, 0, pos, enc));
                    }
                    if ((i < numChars) && (c == '%')) {
                        sb.append(c);
                        i++;
                    }
                    break;
                default:
                    sb.append(c);
                    i++;
                    break;
            }
        }
        return sb.toString();
    }

    private static String asciiToLower(String s) {
        char[] c = new char[s.length()];
        s.getChars(0, s.length(), c, 0);
        int d = 'a' - 'A';
        for (int i = 0; i < c.length; i++) {
            if (c[i] >= 'A' && c[i] <= 'Z') {
                c[i] = (char) (c[i] + d);
            }
        }
        return new String(c);
    }


    private static String byteArrayToHex(final byte[] digest) {
        final StringBuilder ret = new StringBuilder(digest.length * 2);
        String tmp;
        for (final byte d : digest) {
            tmp = Integer.toHexString(d & 0xFF);
            if (tmp.length() < 2) {
                ret.append('0');
            }
            ret.append(tmp);
        }
        return ret.toString();
    }

    private String tokenType;

    private String api_getTokenType(Account account, String token, boolean liveToken) throws Exception {
        if (token == null) {
            synchronized (account) {
                try {
                    String tokenType = account.getStringProperty("tokenType", null);
                    if (tokenType != null && liveToken == false) {
                        return tokenType;
                    }
                    Client client = new Client();
                    String url = getProtocol() + "api.uploaded.net/api/user/jdownloader?access_token=" + token;
                    Response response = client.makeGetRequest(url);
                    String res = response.body().string();
                    JSONObject jsonObject = new JSONObject(res);
                    tokenType = jsonObject.getString("account_type");

                    //tokenType = br.getRegex("account_type\":\\s*?\"(premium|free)").getMatch(0);
                    if (tokenType == null) {

                    }
                    account.setProperty("tokenType", tokenType);
                    if ("premium".equals(tokenType)) {
                        try {
                            maxPrem.set(ACCOUNT_PREMIUM_MAXDOWNLOADS);
                            account.setMaxSimultanDownloads(ACCOUNT_PREMIUM_MAXDOWNLOADS);
                            account.setConcurrentUsePossible(ACCOUNT_PREMIUM_CONCURRENT_USAGE_POSSIBLE);
                        } catch (final Throwable e) {
                        }
                    } else {
                        try {
                            maxPrem.set(ACCOUNT_FREE_MAXDOWNLOADS);
                            account.setMaxSimultanDownloads(ACCOUNT_FREE_MAXDOWNLOADS);
                            account.setConcurrentUsePossible(ACCOUNT_FREE_CONCURRENT_USAGE_POSSIBLE);
                        } catch (final Throwable e) {
                        }
                    }
                    this.tokenType = tokenType;
                    return tokenType;
                } catch (final Exception e) {

                    throw e;
                }
            }
        } else {
            return this.tokenType;
        }
    }


    public DownloadLink checkIfLinkValid(Account account, DownloadLink downloadLink) {
        String token = null;
        try {
            token = api_getAccessToken(account, false);

            String tokenType = api_getTokenType(account, token, false);
            String id = getID(downloadLink);
            String urlPost = getProtocol() + "api.uploaded.net/api/download/jdownloader";
            RequestBody requestBody = new FormBody.Builder().
                    addEncoded("access_token", token)
                    .addEncoded("auth", id)
                    .build();

            Client client = new Client();
            Response response = client.makePost(urlPost, requestBody);
            String rest = response.body().string();
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                com.rajwa.debird.Model.DTO.Response response1 = objectMapper.readValue(rest, com.rajwa.debird.Model.DTO.Response.class);
                downloadLink.setName(response1.getName());
                downloadLink.setSizeInBytes(Integer.parseInt(response1.getSize()));
                downloadLink.setAvailableStatus(DownloadLink.AvailableStatus.TRUE);
                return downloadLink;
            } catch (Exception ex) {
                downloadLink.setAvailableStatus(DownloadLink.AvailableStatus.FALSE);
                return downloadLink;
            }
//        if (rest.contains("err")) {
//            return null;
//        }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        }
        catch (Exception ex){
            logger.error(ex.getMessage());
        }
        downloadLink.setAvailableStatus(DownloadLink.AvailableStatus.FALSE);
        return downloadLink;
    }



    public void api_handle_Premium(final DownloadLink downloadLink, final Account account) throws Exception {
        correctDownloadLink(downloadLink);
        String token = api_getAccessToken(account, false);
        String tokenType = api_getTokenType(account, token, false);
        if (!"premium".equals(tokenType)) {
           // site_login(account, false);
          //  doFree(downloadLink, account);
            return;
        }
        logger.info("Premium Account, API download method in use!");
        String id = getID(downloadLink);
        //postPage(br, getProtocol() + "api.uploaded.net/api/download/jdownloader", "access_token=" + token + "&auth=" + id);
        String urlPost = getProtocol() + "api.uploaded.net/api/download/jdownloader";
        RequestBody requestBody = new FormBody.Builder().
                addEncoded("access_token",token)
                .addEncoded("auth",id)
                .build();
        Client client = new Client();
        Response response = client.makePost(urlPost,requestBody);
        String rest = response.body().string();
        logger.info(rest);

        JSONObject jsonObject = new JSONObject(rest);
        String link = jsonObject.getString("link").replaceAll("\\\\","");
        link = link + "?access_token=" + token;
        String fileName = jsonObject.getString("name");
        Response response1 = client.makeGetRequest(link);
        long fileSize= response1.body().contentLength();
        BufferedSource source = response1.body().source();
        Integer DOWNLOAD_CHUNK_SIZE = 2048;


        File targetFile = new File("C:\\Users\\Dominik\\Desktop" + File.separator + fileName);
        BufferedSink sink = Okio.buffer(Okio.sink(targetFile));
        OutputStream outStream = new FileOutputStream(targetFile);
        long totalRead =0;
        long read = 0;
        while ((read = source.read(sink.buffer(),DOWNLOAD_CHUNK_SIZE)) != -1) {
            totalRead += read;
            int progress = (int) ((totalRead * 100) / fileSize);
            logger.info(String.valueOf(progress) +"%");
        }
        sink.writeAll(source);
        sink.flush();
        sink.close();
//        if (br.containsHTML("\"err\":\\{\"code\":403")) {
//            downloadLink.setProperty("preDlPass", true);
//            throw new PluginException(LinkStatus.ERROR_RETRY);
//        }
//        String url = br.getRegex("link\":\\s*?\"(http.*?)\"").getMatch(0);
//        if (url == null) {
//            url = br.getRegex("link\":\\s*?\"(\\\\/dl.*?)\"").getMatch(0);
//            if (url != null) {
//                throw new PluginException(LinkStatus.ERROR_TEMPORARILY_UNAVAILABLE, "Download currently not possible", 20 * 60 * 1000l);
//            }
//        }
//        String sha1 = br.getRegex("sha1\":\\s*?\"([0-9a-fA-F]+)\"").getMatch(0);
//        String name = br.getRegex("name\":\\s*?\"(.*?)\"").getMatch(0);
//        String size = br.getRegex("size\":\\s*?\"?(\\d+)\"").getMatch(0);
//        String concurrent = br.getRegex("concurrent\":\\s*?\"?(\\d+)").getMatch(0);
//        if (url == null) {
//            handleErrorCode(br, account, token, true);
//        }
//        if (sha1 != null) {
//            downloadLink.setSha1Hash(sha1);
//        }
//        setFinalFileName(downloadLink, name);
//        if (downloadLink.getVerifiedFileSize() == -1 && size != null) {
//            downloadLink.setVerifiedFileSize(Long.parseLong(size));
//        }
//        url = url.replaceAll("\\\\/", "/");
//        /* we must append access_token because without the url won't work */
//        url = url + "?access_token=" + token;
//        int maxChunks = 0;
//        if (concurrent != null) {
//            int maxConcurrent = Math.abs(Integer.parseInt(concurrent));
//            if (maxConcurrent == 1) {
//                maxChunks = 1;
//            } else if (maxConcurrent > 1) {
//                maxChunks = -maxConcurrent;
//            }
//        }
//        dl = jd.plugins.BrowserAdapter.openDownload(br, downloadLink, url, true, maxChunks);
//        try {
//            /* remove next major update */
//            /* workaround for broken timeout in 0.9xx public */
//            ((RAFDownload) dl).getRequest().setConnectTimeout(30000);
//            ((RAFDownload) dl).getRequest().setReadTimeout(60000);
//        } catch (final Throwable ee) {
//        }
//        handleGeneralServerErrors();
//        if (dl.getConnection().getLongContentLength() == 0 || !dl.getConnection().isContentDisposition()) {
//            try {
//                br.followConnection();
//            } catch (final Throwable e) {
//                logger.severe(e.getMessage());
//            }
//            handleErrorCode(br, account, token, false);
//            checkGeneralErrors();
//            try {
//                logger.info(br.toString());
//            } catch (final Throwable e) {
//            }
//            try {
//                logger.info(dl.getConnection().toString());
//            } catch (final Throwable e) {
//            }
//            if ("No htmlCode read".equalsIgnoreCase(br.toString())) {
//                throw new PluginException(LinkStatus.ERROR_TEMPORARILY_UNAVAILABLE, "ServerError", 30 * 60 * 1000l);
//            }
//            if (br.containsHTML("You used too many different IPs, Downloads have been blocked for today\\.")) {
//                // shown in html of the download server, 'You used too many different IPs, Downloads have been blocked for today.'
//                logger.warning("Your account has been disabled due account access from too many different IP addresses, Please contact " + this.getHost() + " support for resolution.");
//                throw new PluginException(LinkStatus.ERROR_PREMIUM, "Your account has been disabled due account access from too many different IP addresses, Please contact " + this.getHost() + " support for resolution.", PluginException.VALUE_ID_PREMIUM_DISABLE);
//            } else if (br.containsHTML("We're sorry but your download ticket couldn't have been found")) {
//                throw new PluginException(LinkStatus.ERROR_TEMPORARILY_UNAVAILABLE, "ServerError", 5 * 60 * 1000l);
//            }
//            // unknown error/defect, lets try next time with web method!
//            usePremiumAPI.set(false);
//            throw new PluginException(LinkStatus.ERROR_PLUGIN_DEFECT);
//        }
//        if (dl.getConnection().getResponseCode() == 404) {
//            try {
//                br.followConnection();
//            } catch (final Throwable e) {
//                logger.severe(e.getMessage());
//            }
//            // this does not mean that the file is offline. This is most likly a server error. try again. if the file is really offline, the
//            // linkcheck will set the corrects status
//            throw new PluginException(LinkStatus.ERROR_TEMPORARILY_UNAVAILABLE, "ServerError(404)", 1 * 60 * 1000l);
//        }
//        if (downloadLink.getBooleanProperty(UPLOADED_FINAL_FILENAME, false) == false) {
//            final String uploadedFinalFileName = Plugin.getFileNameFromDispositionHeader(dl.getConnection());
//            if (StringUtils.isNotEmpty(uploadedFinalFileName)) {
//                downloadLink.setFinalFileName(uploadedFinalFileName);
//                downloadLink.setProperty(UPLOADED_FINAL_FILENAME, true);
//            } else if (StringUtils.isNotEmpty(name)) {
//                downloadLink.setFinalFileName(name);
//                downloadLink.setProperty(UPLOADED_FINAL_FILENAME, true);
//            }
//        }
//        dl.startDownload();
    }

    public boolean correctDownloadLink(final DownloadLink link) {
        String protocol = new Regex(link.getDownloadURL(), "(https?)://").getMatch(0);
        String id = getID(link);
        //link.setLinkID(getHost() + "://" + id);
        link.setUrlDownload(protocol + "://uploaded.net/file/" + id);
        return true;
    }

    private String getID(final DownloadLink downloadLink) {
        String id = new Regex(downloadLink.getDownloadURL(), "/file/([\\w]+)/?").getMatch(0);
        if (id != null) {
            return id;
        }
        id = new Regex(downloadLink.getDownloadURL(), "\\?id=([\\w]+)/?").getMatch(0);
        if (id != null) {
            return id;
        }
        id = new Regex(downloadLink.getDownloadURL(), "(\\.net|\\.to)/([\\w]+)/?").getMatch(1);
        return id;
    }

}
