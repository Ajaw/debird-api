package com.rajwa.debird.Download;

import java.net.URLEncoder;
/**
 * Created by Dominik on 08.07.2018.
 */
public class Encoding {

    public static String urlEncode(final String str) {
        if (str == null) {
            return null;
        }
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (final Exception e) {

        }
        return str;
    }
}
