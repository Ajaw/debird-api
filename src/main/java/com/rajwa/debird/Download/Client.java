package com.rajwa.debird.Download;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by Dominik on 08.07.2018.
 */
public class Client {

    private Logger logger = LoggerFactory.getLogger(Client.class);

    private OkHttpClient httpClient;


    private void checkIfClientExitsts(){
        if (httpClient==null){
            httpClient = new OkHttpClient();
        }
    }

    public Response makeGetRequest(String url){
        checkIfClientExitsts();
        Request request =  new Request.Builder()
                .url(url)
                .build();
        Response response = null;
        try {
            response = httpClient.newCall(request)
                    .execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public Response makePost(String url, RequestBody requestBody){
        checkIfClientExitsts();
        Request request =  new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        Response response = null;
        try {
            response = httpClient.newCall(request)
                    .execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }




}
