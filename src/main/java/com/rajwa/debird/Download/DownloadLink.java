package com.rajwa.debird.Download;

/**
 * Created by Dominik on 08.07.2018.
 */
//    jDownloader - Downloadmanager
//    Copyright (C) 2008  JD-Team support@jdownloader.org
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import java.util.List;


/**
 * Hier werden alle notwendigen Informationen zu einem einzelnen Download festgehalten. Die Informationen werden dann in einer Tabelle
 * dargestellt
 *
 * @author astaldo
 */
public class DownloadLink  {
    public static enum AvailableStatus {
        UNCHECKED("Unchecked"),
        FALSE("Offline"),
        UNCHECKABLE("Unkceckable"),
        TRUE("Online");
        private final String exp;

        private AvailableStatus(String exp) {
            this.exp = exp;
        }

        public String getExplanation() {
            return exp;
        }
    }

    private static final String                         PROPERTY_MD5                        = "MD5";
    private static final String                         PROPERTY_HASHINFO                   = "HASHINFO";
    private static final String                         PROPERTY_MIRRORID                   = "MID";
    private static final String                         PROPERTY_SHA1                       = "SHA1";
    private static final String                         PROPERTY_SHA256                     = "SHA256";
    private static final String                         PROPERTY_PASS                       = "pass";
    private static final String                         PROPERTY_FINALFILENAME              = "FINAL_FILENAME";
    private static final String                         PROPERTY_FORCEDFILENAME             = "FORCED_FILENAME";
    private static final String                         PROPERTY_COMMENT                    = "COMMENT";
    private static final String                         PROPERTY_PRIORITY                   = "PRIORITY2";
    private static final String                         PROPERTY_FINISHTIME                 = "FINISHTIME";
    private static final String                         PROPERTY_PWLIST                     = "PWLIST";
    private static final String                         PROPERTY_LINKDUPEID                 = "LINKDUPEID";
    private static final String                         PROPERTY_SPEEDLIMIT                 = "SPEEDLIMIT";
    private static final String                         PROPERTY_VERIFIEDFILESIZE           = "VERIFIEDFILESIZE";
    public static final String                          PROPERTY_RESUMEABLE                 = "PROPERTY_RESUMEABLE";
    public static final String                          PROPERTY_CUSTOM_LOCALFILENAME       = "CUSTOM_LOCALFILENAME";
    public static final String                          PROPERTY_CUSTOM_LOCALFILENAMEAPPEND = "CUSTOM_LOCALFILENAMEAPPEND";
    public static final String                          PROPERTY_DOWNLOADTIME               = "DOWNLOADTIME";
    public static final String                          PROPERTY_ARCHIVE_ID                 = "ARCHIVE_ID";
    public static final String                          PROPERTY_EXTRACTION_STATUS          = "EXTRACTION_STATUS";
    public static final String                          PROPERTY_CUSTOM_MESSAGE             = "CUSTOM_MESSAGE";
    public static final String                          PROPERTY_MIME_HINT                  = "MIME_HINT";
    private static final long                           serialVersionUID                    = 1981079856214268373L;
    private static final String                         UNKNOWN_FILE_NAME                   = "unknownFileName";
    private static final String                         PROPERTY_CHUNKS                     = "CHUNKS";
    private static final String                         URL_ORIGIN                          = "URL_ORIGIN";
    private static final String                         URL_REFERRER                        = "URL_REFERRER";
    private static final String                         URL_CONTAINER                       = "URL_CONTAINER";
    private static final String                         URL_CONTENT                         = "URL_CONTENT";
    private static final String                         URL_CUSTOM                          = "URL_CUSTOM";
    private static final String                         VARIANT_SUPPORT                     = "VARIANT_SUPPORT";
    public static final String                          PROPERTY_JOB_ID                     = "JOB_ID";
    private transient volatile AvailableStatus          availableStatus                     = AvailableStatus.UNCHECKED;
    @Deprecated
    private long[]                                      chunksProgress                      = null;
    /** Aktuell heruntergeladene Bytes der Datei */
    private long                                        downloadCurrent                     = 0;

    /** Maximum der heruntergeladenen Datei (Dateilaenge) */
    private long                                        downloadMax                         = -1;

    /** Hoster des Downloads */
    private String                                      host;
    private boolean                                     isEnabled;
    /** Beschreibung des Downloads */
    /* kann sich noch ändern, NICHT final */
    private String                                      name;

    /**
     * Do not rename urlDownload. We need this field to restore old downloadlinks from the jd09 database
     */
    private String                                      urlDownload;

    private long                                        created                             = -1l;

    private transient volatile long                     lastAvailableStatusChange           = -1;

    private transient Boolean                           partOfAnArchive                     = null;
    private transient List<String>                      sourcePluginPasswordList            = null;

    public Integer getSizeInBytes() {
        return sizeInBytes;
    }

    public void setSizeInBytes(Integer sizeInBytes) {
        this.sizeInBytes = sizeInBytes;
    }

    private Integer sizeInBytes;

    public String getDownloadURL() {
        return downloadURL;
    }

    public void setDownloadURL(String downloadURL) {
        this.downloadURL = downloadURL;
    }

    private String downloadURL;

    public AvailableStatus getAvailableStatus() {
        return availableStatus;
    }

    public void setAvailableStatus(AvailableStatus availableStatus) {
        this.availableStatus = availableStatus;
    }

    public long[] getChunksProgress() {
        return chunksProgress;
    }

    public void setChunksProgress(long[] chunksProgress) {
        this.chunksProgress = chunksProgress;
    }

    public long getDownloadCurrent() {
        return downloadCurrent;
    }

    public void setDownloadCurrent(long downloadCurrent) {
        this.downloadCurrent = downloadCurrent;
    }

    public long getDownloadMax() {
        return downloadMax;
    }

    public void setDownloadMax(long downloadMax) {
        this.downloadMax = downloadMax;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrlDownload() {
        return urlDownload;
    }

    public void setUrlDownload(String urlDownload) {
        this.urlDownload = urlDownload;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public long getLastAvailableStatusChange() {
        return lastAvailableStatusChange;
    }

    public void setLastAvailableStatusChange(long lastAvailableStatusChange) {
        this.lastAvailableStatusChange = lastAvailableStatusChange;
    }

    public Boolean getPartOfAnArchive() {
        return partOfAnArchive;
    }

    public void setPartOfAnArchive(Boolean partOfAnArchive) {
        this.partOfAnArchive = partOfAnArchive;
    }

    public List<String> getSourcePluginPasswordList() {
        return sourcePluginPasswordList;
    }

    public void setSourcePluginPasswordList(List<String> sourcePluginPasswordList) {
        this.sourcePluginPasswordList = sourcePluginPasswordList;
    }

    public static String getRelativeDownloadFolderPath() {
        return RELATIVE_DOWNLOAD_FOLDER_PATH;
    }

    public static final String                          RELATIVE_DOWNLOAD_FOLDER_PATH       = "subfolderbyplugin";

}

