package com.rajwa.debird.DAO;

import com.rajwa.debird.Model.DebirdApp.Hoster;
import com.rajwa.debird.Model.DebirdApp.User;
import org.apache.catalina.Host;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface HosterDAO  extends MongoRepository<Hoster,String> {


    Hoster findFirstByRealId(String realId);

    Hoster findFirstByName(String name);

}
