package com.rajwa.debird.DAO;

import com.rajwa.debird.Model.DebirdApp.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

/**
 * Created by Dominik on 08.07.2018.
 */
public interface UserDAO extends MongoRepository<User,String> {




    public User findByEmail(String email);

    public User findByActiveToken(String token);

}
