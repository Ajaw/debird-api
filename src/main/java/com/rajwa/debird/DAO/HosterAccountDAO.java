package com.rajwa.debird.DAO;

import com.rajwa.debird.Model.DebirdApp.Hoster;
import com.rajwa.debird.Model.DebirdApp.HosterAccount;
import org.apache.catalina.Host;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface HosterAccountDAO extends MongoRepository<HosterAccount,String> {

        HosterAccount findFirstByHoster(Hoster hoster);

}
