package com.rajwa.debird.DAO;

import com.rajwa.debird.Model.DebirdApp.Coupon;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CouponDAO extends MongoRepository<Coupon,String> {

    Coupon findFirstByCode(String code);

}
