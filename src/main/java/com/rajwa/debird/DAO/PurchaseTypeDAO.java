package com.rajwa.debird.DAO;

import com.rajwa.debird.Model.DebirdApp.PurchaseType;
import io.swagger.models.auth.In;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PurchaseTypeDAO extends MongoRepository<PurchaseType, String> {

    PurchaseType findFirstByLengthOfSubscription(Integer length);

}
