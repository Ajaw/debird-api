package com.rajwa.debird.DAO;

import com.rajwa.debird.Model.DebirdApp.FileDownload;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface FileDownloadDAO extends MongoRepository<FileDownload, ObjectId> {


    List<FileDownload> findAllByUserToken(String userToken);

}
