package com.rajwa.debird.Controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rajwa.debird.DAO.UserDAO;
import com.rajwa.debird.Model.DTO.AuthDTO;
import com.rajwa.debird.Model.DTO.UserDTO;
import com.rajwa.debird.Model.DebirdApp.Coupon;
import com.rajwa.debird.Model.DebirdApp.User;
import com.rajwa.debird.Utils.SsoUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * Created by Dominik on 08.07.2018.
 */
@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {

    @Autowired
    private UserDAO userDAO;


    @Autowired
    private EmailController emailController;

    @RequestMapping(value = "/getuser",method = RequestMethod.POST,produces = "application/json")
    @ResponseBody
    public boolean getUserByEmail(@RequestParam("email") String email){
        User user = userDAO.findByEmail(email);
        if (user!=null){
            return  true;
        }
        return false;
    }





    @RequestMapping(value = "/logintoapp",method = RequestMethod.POST,produces = "application/json")
    @ResponseBody
    public AuthDTO login(@RequestParam("user") String json){
        ObjectMapper mapper  = new ObjectMapper();
        User user = null;

        try {
            user = mapper.readValue(json,User.class);
            User foundUser = userDAO.findByEmail(user.getUsername());
            if (foundUser.getPassword().equals(SsoUtils.getPasswordHash(user.getPassword()))) {
                user = foundUser;
                user.setActiveToken(SsoUtils.getToken());

                user.setTokenValidUntil(LocalDateTime.now().plusHours(1));
                AuthDTO authDTO = new AuthDTO();
                authDTO.setToken(user.getActiveToken());
                authDTO.setValidUntil(user.getTokenValidUntil());
                userDAO.save(user);
                return authDTO;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        AuthDTO authDTO = new AuthDTO();
        return authDTO;
    }


    @RequestMapping(value= "/createuser",method = RequestMethod.POST,produces = "application/json")
    @ResponseBody
    public boolean createUser(@RequestParam("user") String  json){
        ObjectMapper mapper  = new ObjectMapper();
        User user = null;
        try {
             user = mapper.readValue(json,User.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if  (user!=null && !getUserByEmail(user.getEmail())) {
            Date registrationDate = new Date();
            user.setRegistationDate(registrationDate);
            user.setConfirmed(false);
            user.setEmailToken(SsoUtils.getToken());

            user.setPassword(SsoUtils.getPasswordHash(user.getPassword()));
            user.setModifiedDate(registrationDate);
            userDAO.save(user);
            emailController.sendEmailAboutRegistration(user);
            return true;
        }
        return false;
    }


    public void activateCoupon(User user, Coupon coupon){

    }




}
