package com.rajwa.debird.Controller;

import com.rajwa.debird.DAO.CouponDAO;
import com.rajwa.debird.DAO.PurchaseTypeDAO;
import com.rajwa.debird.DAO.UserDAO;
import com.rajwa.debird.DAO.UserPurchaseDAO;
import com.rajwa.debird.Model.DTO.LinksDTO;
import com.rajwa.debird.Model.DebirdApp.Coupon;
import com.rajwa.debird.Model.DebirdApp.PurchaseType;
import com.rajwa.debird.Model.DebirdApp.User;
import com.rajwa.debird.Model.DebirdApp.UserPurchase;
import com.rajwa.debird.Utils.SsoUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;

@RestController
@RequestMapping(value = "/coupon")
@CrossOrigin(origins = "http://localhost:4200")
public class CouponController {


    @Autowired
    private CouponDAO couponDAO;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private UserPurchaseDAO userPurchaseDAO;

    @Autowired
    private UserController userController;

    @Autowired
    private PurchaseTypeDAO purchaseTypeDAO;

    @RequestMapping(value = "/activate", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public LinksDTO downloadData(@RequestParam("code") String code, @RequestParam("token") String token) {

        Coupon coupon = couponDAO.findFirstByCode(code);

        User user = userDAO.findByActiveToken(token);

        if (coupon!=null && !coupon.isUsed() && user!=null){
            this.useCoupon(coupon,user);
        }

        return new LinksDTO();

    }

    @RequestMapping(value = "/generate", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String generateCoupon(@RequestParam("lengthOfSubscription") Integer lengthOfSubscription,@RequestParam("apiToken") String apiToken) {
        PurchaseType purchaseType = purchaseTypeDAO.findFirstByLengthOfSubscription(lengthOfSubscription);

        Coupon coupon = new Coupon();
        coupon.setCode(SsoUtils.getToken());
        coupon.setPurchaseType(purchaseType);
        couponDAO.save(coupon);

        return "";

    }



    private boolean useCoupon(Coupon coupon, User user){

        PurchaseType purchaseType = coupon.getPurchaseType();
        UserPurchase purchase = new UserPurchase();
        purchase.setActive(true);
        purchase.setPurchaseType(coupon.getPurchaseType());
        purchase.setSubscriptionStart(new Date());
        if (coupon.getPurchaseType().isRecurring()) {
          //  purchase.setSubscriptionEnd(Date.from(LocalDateTime.now().plusDays(coupon.getPurchaseType().getLengthOfSubscription()).toInstant()));
            purchase.setTransferLeft(coupon.getPurchaseType().getTransfer());
            user.setTransferLeft(coupon.getPurchaseType().getTransfer());
        }
        userDAO.save(user);
        userPurchaseDAO.save(purchase);
        couponDAO.save(coupon);
        return true;
    }
}
