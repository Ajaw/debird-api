package com.rajwa.debird.Controller;

import com.rajwa.debird.DAO.UserDAO;
import com.rajwa.debird.Model.DebirdApp.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Dominik on 01.09.2018.
 */

@Service
public class AuthService {

    @Autowired
    private UserDAO userDAO;


    public User checkToken(String  token){
        return userDAO.findByActiveToken(token);
    }

}
