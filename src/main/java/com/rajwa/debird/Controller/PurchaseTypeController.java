package com.rajwa.debird.Controller;

import com.rajwa.debird.DAO.PurchaseTypeDAO;
import com.rajwa.debird.Model.DebirdApp.Coupon;
import com.rajwa.debird.Model.DebirdApp.PurchaseType;
import com.rajwa.debird.Utils.SsoUtils;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/purchaseType")
@CrossOrigin(origins = "http://localhost:4200")
public class PurchaseTypeController {


    @Autowired
    private PurchaseTypeDAO purchaseTypeDAO;

    @RequestMapping(value = "/create", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public void generateCoupon(@RequestParam("lengthOfSubscription") Integer lengthOfSubscription, @RequestParam("price") Double price) {
            PurchaseType purchaseType = new PurchaseType();
            purchaseType.setLengthOfSubscription(lengthOfSubscription);
            purchaseType.setPrice(Double.valueOf(price));
            purchaseType.setRecurring(false);
            purchaseTypeDAO.save(purchaseType);

    }

}
