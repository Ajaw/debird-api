package com.rajwa.debird.Controller;

import com.rajwa.debird.Model.DebirdApp.User;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfig;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Dominik on 26.08.2018.
 */
@Service
public class EmailTemplatingEngine {


    @Autowired
    private Configuration freemakerConfig;

    public String getRegistartinoMessage(User user){

        try {
            Template template = freemakerConfig.getTemplate("/email/registration.ftl");
            Map root =new HashMap();
            root.put("myUser",user);

            String html = FreeMarkerTemplateUtils.processTemplateIntoString(template,root);
            return html;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TemplateException e) {
            e.printStackTrace();
        }
        return null;

    }

}
