package com.rajwa.debird.Controller;

import com.rajwa.debird.DAO.HosterAccountDAO;
import com.rajwa.debird.DAO.HosterDAO;
import com.rajwa.debird.Model.DTO.LinksDTO;
import com.rajwa.debird.Model.DebirdApp.Hoster;
import com.rajwa.debird.Model.DebirdApp.HosterAccount;
import com.rajwa.debird.Utils.SsoUtils;
import org.apache.commons.io.FilenameUtils;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

@RestController
public class HosterController {

    @Autowired
    private HosterDAO hosterDAO;

    @Autowired
    private HosterAccountDAO hosterAccountDAO;

    @Value("${app.data.path}")
    private String appDataPath;

    private Logger logger = LoggerFactory.getLogger(HosterController.class);

    @RequestMapping(value = "/gethoster",method = RequestMethod.GET,produces = "application/json")
    @ResponseBody
    public Hoster getHoster(){
        Hoster hoster = new Hoster();
        hoster.setActive(false);
        hoster.setDisplay(true);
        hoster.setPathToImage("test");
        hoster.setTotalTransferLeft(10000.0);
        hoster.setRegex("test");
        hoster.setName("test");
        hoster.setHosterAccountList(new LinkedList<>());
        hosterDAO.save(hoster);
        hoster.setRealId(hoster.getId().toHexString());
        hosterDAO.save(hoster);
        return hoster;
    }

    @RequestMapping(value = "/createhoster",method = RequestMethod.POST,produces = "application/json")
    @ResponseBody
    public Hoster createHoster(@RequestParam("image")MultipartFile file,
                               @RequestParam("name") String name){
        Hoster hoster = null;
        hoster = hosterDAO.findFirstByName(name);
        if (hoster==null)
            hoster = new Hoster();
        hoster.setActive(true);
        hoster.setDisplay(true);
        hoster.setPathToImage("test");
        hoster.setTotalTransferLeft(10000.0);
        File dataPath = new File(this.appDataPath + File.separator + name +  "." + FilenameUtils.getExtension(file.getOriginalFilename()));
        try {
            file.transferTo(dataPath);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        //hoster.setRegex("test");
        hoster.setName(name);
        hoster.setPathToImage(dataPath.getName());
        hoster.setHosterAccountList(new LinkedList<>());
        hosterDAO.save(hoster);
        hoster.setRealId(hoster.getId().toHexString());
        hosterDAO.save(hoster);
        return hoster;
    }


    @RequestMapping(value = "/createhosteraccount",method = RequestMethod.GET,produces = "application/json")
    @ResponseBody
    public HosterAccount createHosterAccount(@RequestParam("id") String  id, @RequestParam("username") String username, @RequestParam("password") String password){
        Hoster hoster = hosterDAO.findFirstByRealId(id);
        if (hoster!=null){
            HosterAccount hosterAccount = new HosterAccount();
            hosterAccount.setHoster(hoster);
            hosterAccount.setPassword(password);
            hosterAccount.setUsername(username);
            hosterAccount.setQueueName(SsoUtils.getToken()+hoster.getName());
            //hoster.getHosterAccountList().add(hosterAccount);
            //hosterDAO.save(hoster);
            hosterAccountDAO.save(hosterAccount);
            return hosterAccount;
        }

        return null;

    }


}
