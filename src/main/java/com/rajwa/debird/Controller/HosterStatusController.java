package com.rajwa.debird.Controller;

import com.rajwa.debird.DAO.HosterDAO;
import com.rajwa.debird.Model.DebirdApp.Hoster;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping(value = "/hoster")
@CrossOrigin(origins = "http://localhost:4200")
public class HosterStatusController {

    @Autowired
    private HosterDAO hosterDAO;

    @RequestMapping(value = "/status",method = RequestMethod.GET,produces = "application/json")
    @ResponseBody
    public List<Hoster> getHoster(){
        return hosterDAO.findAll();
    }




}


