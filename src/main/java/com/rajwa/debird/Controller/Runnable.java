package com.rajwa.debird.Controller;

import com.rajwa.debird.Model.ProcessState;
import com.rajwa.debird.Model.ProcessStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

public class Runnable implements java.lang.Runnable {

    private Logger logger = LoggerFactory.getLogger(Runnable.class);

    private AtomicInteger status = new AtomicInteger(0);
    private String processId;

    public AtomicInteger getStatus() {
        return status;
    }

    public void setStatus(AtomicInteger status) {
        this.status = status;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public SimpMessagingTemplate getTemplate() {
        return template;
    }

    public void setTemplate(SimpMessagingTemplate template) {
        this.template = template;
    }

    private Date dateTime;
    private Integer state;
    private SimpMessagingTemplate template;

    /**
     * Updates status/progress to registered clients.
     * Javascript client code for subscribing to serverside messages:
     * stompClient.subscribe('/topic/progress', callBackFuntion);
     *
     * @param updateBy
     * @param message
     */
    private void updateStatus(int updateBy, String message) {
        if (0==updateBy) {
            status.set(0);
        }
        ProcessStatus processStatus = new ProcessStatus();
        processStatus.setMessage(message);
        processStatus.setPrecentComplete(status.addAndGet(updateBy));
        processStatus.setId(this.processId);
        template.convertAndSend("/topic/progress", processStatus);
        logger.info("update status");
    }


    public void run()  {

        updateStatus(0, "");
        updateStatus(10, "Initializing");
        try {
            Thread.sleep(5000);
            updateStatus(10, "Done");
            Thread.sleep(5000);
            updateStatus(20, "Done");
            Thread.sleep(5000);
            updateStatus(20, "Done");
            Thread.sleep(5000);
            updateStatus(20, "Done");
            Thread.sleep(5000);
            updateStatus(20, "Finished");
        } catch (InterruptedException e) {
            logger.error("Thread interrupt error", e);
            updateStatus(0, "");
        }
        this.state = ProcessState.STATE_FINISHED;
    }

}
