package com.rajwa.debird.Controller;

import com.rajwa.debird.DAO.HosterAccountDAO;
import com.rajwa.debird.DAO.HosterDAO;
import com.rajwa.debird.Download.DownloadLink;
import com.rajwa.debird.Download.Hosters.Uploadedto;
import com.rajwa.debird.Model.DTO.LinkDTO;
import com.rajwa.debird.Model.DTO.LinksDTO;
import com.rajwa.debird.Model.DebirdApp.Hoster;
import com.rajwa.debird.Model.DebirdApp.HosterAccount;
import com.rajwa.debird.Model.DebirdApp.User;
import com.rajwa.debird.Model.JD.Account;
import com.rajwa.debird.Model.ProcessState;
import com.rajwa.debird.Model.ProcessStatus;
import com.rajwa.debird.Utils.SsoUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitMessagingTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Dominik on 01.09.2018.
 */

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class DownloadController {


    @Autowired
    private AuthService authService;

    @Autowired
    private Uploadedto uploadedto;

    @Autowired
    private MappingJackson2MessageConverter mappingJackson2MessageConverter;

    @Autowired
    private RabbitMessagingTemplate messagingTemplate;

    @Autowired
    private HosterDAO hosterDAO;

    @Autowired
    private HosterAccountDAO hosterAccountDAO;

    private Logger logger = LoggerFactory.getLogger(DownloadController.class);


    @RequestMapping(value = "/download",method = RequestMethod.GET,produces = "application/json")
    @ResponseBody
    public LinksDTO downloadData(@RequestParam("links") String links,@RequestParam("token") String token){
        this.messagingTemplate.setMessageConverter(mappingJackson2MessageConverter);
        User user = authService.checkToken(token);
        if (user!=null){
            String[] linkArray = links.split("\n");
            List<String> linkList = Arrays.asList(linkArray);
            LinksDTO linksDTO = new LinksDTO();
            List<LinkDTO> linkDTOList = new LinkedList<>();
            for (String s : linkList) {
                LinkDTO linkDTO = new LinkDTO();
                linkDTO.setHosterId(1);
                linkDTO.setLink(s);
                DownloadLink link = new DownloadLink();
                link.setDownloadURL(s);
                link.setAvailableStatus(DownloadLink.AvailableStatus.TRUE);
               // link = uploadedto.checkIfLinkValid(getAccount(),link);

                if (link.getAvailableStatus().equals(DownloadLink.AvailableStatus.TRUE)){
                    linkDTO.setStatus(true);
                   // linkDTO.setFileSize(this.convertSize(link.getSizeInBytes().toString()));
                    linkDTO.setFileName("test");
                    linkDTO.setFileToken(SsoUtils.getToken());
                    linkDTO.setUserToken(user.getEmailToken());
                    List<Hoster> hosters = hosterDAO.findAll();
                    HosterAccount hosterAccount =  hosterAccountDAO.findFirstByHoster(hosters.get(0));
                    this.messagingTemplate.convertAndSend("exchange",hosterAccount.getQueueName(),linkDTO);
                }
                else {
                    linkDTO.setStatus(false);
                }



                linkDTOList.add(linkDTO);
            }

            linksDTO.setLinkDTOList(linkDTOList);
            return linksDTO;
        }
        return new LinksDTO();
    }

    @RequestMapping(value = "/checklinks",method = RequestMethod.GET,produces = "application/json")
    @ResponseBody
    public LinksDTO checkLinks(@RequestParam("links") String links,@RequestParam("token") String token){

        User user = authService.checkToken(token);
        if (user!=null){
            String[] linkArray = links.split("\n");
             List<String> linkList = Arrays.asList(linkArray);
            LinksDTO linksDTO = new LinksDTO();
            List<LinkDTO> linkDTOList = new LinkedList<>();
            for (String s : linkList) {
                LinkDTO linkDTO = new LinkDTO();
                linkDTO.setHosterId(1);
                linkDTO.setLink(s);
                DownloadLink link = new DownloadLink();
                link.setDownloadURL(s);
              //  link = uploadedto.checkIfLinkValid(getAccount(),link);
//                if (link.getAvailableStatus().equals(DownloadLink.AvailableStatus.TRUE)){
//                    linkDTO.setStatus(true);
//                    linkDTO.setFileSize(this.convertSize(link.getSizeInBytes().toString()));
//                    linkDTO.setFileName(link.getName());
//                }
//                else {
//                    linkDTO.setStatus(false);
//                }
                linkDTO.setStatus(true);
                linkDTO.setUserToken(user.getEmailToken());

                linkDTO.setFileToken(String.valueOf(link.getDownloadURL().hashCode()));




                linkDTOList.add(linkDTO);
            }
            linksDTO.setLinkDTOList(linkDTOList);
            return linksDTO;
        }
        return new LinksDTO();

    }

    private String convertSize(String size){
        Integer realSize=  Integer.parseInt(size);
        String[] sizes = new String[]{"Bytes", "KB", "MB", "GB", "TB"};

            Double val =  Math.floor(Math.log(realSize)/Math.log(1024));
            Integer i = val.intValue();
            return Math.round(realSize/Math.pow(1024,i))+ " "+sizes[i];

    }







    private Account getAccount(){
        Account account= new Account();
        account.setUser("16724187");
        account.setPass("edj397yhw");
        return account;
    }

}
