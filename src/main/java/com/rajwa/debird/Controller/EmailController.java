package com.rajwa.debird.Controller;

import com.rajwa.debird.Model.DebirdApp.User;
import net.sargue.mailgun.Configuration;
import net.sargue.mailgun.Mail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * Created by Dominik on 26.08.2018.
 */
@Service
public class EmailController {


    @Autowired
    private EmailTemplatingEngine emailTemplatingEngine;

    private Configuration configuration;

    @PostConstruct
    private void setupConfig(){
         configuration = new Configuration()
                .domain("test.com")
                .apiKey("test-key")
                .from("test account","debird@test.com");
    }

    public void sendEmailAboutRegistration(User user){
         String html = emailTemplatingEngine.getRegistartinoMessage(user);
        this.sendEmail(user,"Activation",html);
    }


    private void sendEmail(User user,String subject, String  text){

        Mail.using(configuration)
                .to(user.getEmail())
                .subject(subject)
                .text(text)
                .build().sendAsync();


    }

}
