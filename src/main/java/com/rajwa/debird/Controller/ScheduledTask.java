package com.rajwa.debird.Controller;

import com.rajwa.debird.DAO.FileDownloadDAO;
import com.rajwa.debird.DAO.HosterAccountDAO;
import com.rajwa.debird.DAO.UserDAO;
import com.rajwa.debird.Model.DebirdApp.FileDownload;
import com.rajwa.debird.Model.DebirdApp.User;
import com.rajwa.debird.Model.ProcessState;
import com.rajwa.debird.Model.ProcessStatus;
import com.rajwa.debird.Utils.SsoUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
@Controller
public class ScheduledTask {

    private Set<Runnable> work = new HashSet<Runnable>();

    private Logger logger = LoggerFactory.getLogger(ScheduledTask.class);

    @Autowired
    @Qualifier("getAsyncExecutor") //no mistakes which bean to get
    private ThreadPoolTaskExecutor executor;

    @Autowired
    private SimpMessagingTemplate template;
    @Autowired
    private FileDownloadDAO fileDownloadDAO;

    @Autowired
    private UserDAO userDao;

    @MessageMapping("/send/message")
    private void  onReciveMessage(String message){

        User user = userDao.findByActiveToken(message);
        List<FileDownload> fileDownloads = fileDownloadDAO.findAllByUserToken(user.getEmailToken());
        for (FileDownload fileDownload : fileDownloads) {


        try {
            ProcessStatus processStatus = new ProcessStatus();
            processStatus.setMessage("Done");
            processStatus.setPrecentComplete(fileDownload.getPercentageComplete());
            processStatus.setId(fileDownload.getFileToken());


            template.convertAndSend("/topic/progress", processStatus);
            // template.convertAndSend("/topic/progress", runnable);
        }
        catch (Exception ex){
            logger.error(ex.getMessage());
        }
        }
    }


//    @Scheduled(fixedRate = 20000)
    public void run() {
        logger.info("running task");
        for (int i = 0; i < 1; i++) {
            Runnable process = new Runnable();
            process.setProcessId(SsoUtils.getToken());
            process.setTemplate(template);
            work.add(process);
        }
        List<Runnable> toRemove = new LinkedList<>();
        for (Runnable runnable : work) {
            if (runnable.getState()!=null && runnable.getState().equals(ProcessState.STATE_FINISHED)){
                toRemove.add(runnable);
            }
            else
                executor.execute(runnable);
        }

        for (Runnable runnable : toRemove) {
            //work.remove(runnable);
        }

    }

}
