package com.rajwa.debird.Config;

import com.rajwa.debird.DAO.UserDAO;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * Created by Dominik on 08.07.2018.
 */
@EnableMongoRepositories(basePackageClasses = UserDAO.class)
@Configuration
public class MongoDbConfig {

}
