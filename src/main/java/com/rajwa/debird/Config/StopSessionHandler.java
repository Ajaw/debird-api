package com.rajwa.debird.Config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rajwa.debird.Model.DTO.LinkDTO;
import com.rajwa.debird.Model.ProcessStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.stomp.*;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.lang.reflect.Type;
@Component
public class StopSessionHandler extends StompSessionHandlerAdapter {


    private final SimpMessagingTemplate template;

    private Logger logger = LoggerFactory.getLogger(StopSessionHandler.class);

    @Autowired
    public StopSessionHandler(SimpMessagingTemplate template) {
        this.template = template;
    }

    @Override
    public void afterConnected(StompSession stompSession, StompHeaders stompHeaders) {
        stompSession.subscribe("/topic/progress",this);
    }

    @Override
    public void handleException(StompSession stompSession, StompCommand stompCommand, StompHeaders stompHeaders, byte[] bytes, Throwable throwable) {
        logger.info(throwable.getMessage());
    }

    @Override
    public void handleTransportError(StompSession stompSession, Throwable throwable) {
        logger.info(throwable.getMessage());
    }

    @Override
    public Type getPayloadType(StompHeaders stompHeaders) {
        return ProcessStatus.class;
    }

    @Override
    public void handleFrame(StompHeaders stompHeaders, Object o) {
        ProcessStatus data = (ProcessStatus) o;
        logger.info("recived data");
        ObjectMapper objectMapper = new ObjectMapper();
        try {
           // ProcessStatus status = objectMapper.readValue(data, ProcessStatus.class);

            template.convertAndSend("/topic/progress", data);
        } catch (Exception e) {
            e.printStackTrace();
        }




    }
}
