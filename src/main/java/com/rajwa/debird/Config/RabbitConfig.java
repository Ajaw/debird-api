package com.rajwa.debird.Config;


import com.rabbitmq.client.ConnectionFactoryConfigurator;
import com.rajwa.debird.DAO.HosterAccountDAO;
import com.rajwa.debird.Model.DebirdApp.HosterAccount;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;

import java.util.LinkedList;
import java.util.List;

@EnableRabbit
@Configuration
public class RabbitConfig {



    @Autowired
    private HosterAccountDAO hosterAccountDAO;


    @Bean
    List<Queue> queueMessage() {
        List<HosterAccount> hosterAccounts = hosterAccountDAO.findAll();
        List<Queue> queues = new LinkedList<>();
        for (HosterAccount hosterAccount : hosterAccounts) {
            queues.add(new Queue(hosterAccount.getQueueName(),true));
        }
        return queues;
    }

    @Bean
    TopicExchange exchange(){
        return new TopicExchange("exchange");
    }

    @Bean
    List<Binding> bindingExchangeMessage(List<Queue> queueMessage, TopicExchange exchange){
        List<Binding> bindings = new LinkedList<>();
        for (Queue queue : queueMessage) {

           bindings.add( BindingBuilder.bind(queue).to(exchange).with(queue.getName()));

        }
        return bindings;
    }

    @Bean
    public ConnectionFactory connectionFactory(@Value("${spring.rabbitmq.host}") String hosts,
                                               @Value("${spring.rabbitmq.username}") String username,
                                               @Value("${spring.rabbitmq.password}") String password){
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
        connectionFactory.setUsername(username);
        connectionFactory.setPassword(password);
        connectionFactory.setAddresses(hosts);

        return connectionFactory;
    }

    @Bean
    public MappingJackson2MessageConverter jackson2Converter(){
        return new MappingJackson2MessageConverter();
    }

}
