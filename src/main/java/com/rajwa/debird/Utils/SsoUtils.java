package com.rajwa.debird.Utils;

import org.apache.commons.codec.binary.Hex;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * Created by Dominik on 26.08.2018.
 */
public class SsoUtils {


    public static String getTokenUrl(){
        return "http://localhost:4200/activate?token=";
    }

    public static String getToken(){
        SecureRandom random = new SecureRandom();
        byte bytes[] = new byte[20];
        random.nextBytes(bytes);
        return new String(Hex.encodeHex(bytes));
    }


    private static String salt = "testSaltRajwa123";

    public static String getPasswordHash(String password){
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
            password = salt + password;
        byte[] hash = digest.digest(password.getBytes(StandardCharsets.UTF_8));
        return new String(Hex.encodeHex(hash));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

}
