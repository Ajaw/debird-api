package com.rajwa.debird.Model.DebirdApp;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by Dominik on 08.07.2018.
 */
@Document
public class PurchaseType {

    private ObjectId id;
    private Integer lengthOfSubscription;
    private Double price;

    public boolean isRecurring() {
        return isRecurring;
    }

    public void setRecurring(boolean recurring) {
        isRecurring = recurring;
    }

    private boolean isRecurring;


    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public Integer getLengthOfSubscription() {
        return lengthOfSubscription;
    }

    public void setLengthOfSubscription(Integer lengthOfSubscription) {
        this.lengthOfSubscription = lengthOfSubscription;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getTransfer() {
        return transfer;
    }

    public void setTransfer(Double transfer) {
        this.transfer = transfer;
    }

    private Double transfer;



}
