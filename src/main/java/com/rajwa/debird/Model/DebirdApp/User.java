package com.rajwa.debird.Model.DebirdApp;

import com.rajwa.debird.Utils.SsoUtils;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * Created by Dominik on 08.07.2018.
 */
@Document
public class User {
    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getRegistationDate() {
        return registationDate;
    }

    public void setRegistationDate(Date registationDate) {
        this.registationDate = registationDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Id
    private ObjectId id;
    @Indexed(unique = true)
    private String email;
    private String password;
    private String username;
    private Date registationDate;
    private Date modifiedDate;
    private String emailToken;
    private String passwordResetToken;
    private LocalDateTime emailTokenVaild;
    private Date subscriptionEnd;

    public Date getSubscriptionEnd() {
        return subscriptionEnd;
    }

    public void setSubscriptionEnd(Date subscriptionEnd) {
        this.subscriptionEnd = subscriptionEnd;
    }

    public boolean isPremium() {
        return premium;
    }

    public void setPremium(boolean premium) {
        this.premium = premium;
    }

    private boolean premium;

    public Double getTransferLeft() {
        return transferLeft;
    }

    public void setTransferLeft(Double transferLeft) {
        this.transferLeft = transferLeft;
    }

    private Double transferLeft;

    public String getEmailToken() {
        return emailToken;
    }

    public void setEmailToken(String emailToken) {
        this.emailToken = emailToken;
    }

    public String getPasswordResetToken() {
        return passwordResetToken;
    }

    public void setPasswordResetToken(String passwordResetToken) {
        this.passwordResetToken = passwordResetToken;
    }

    public LocalDateTime getEmailTokenVaild() {
        return emailTokenVaild;
    }

    public void setEmailTokenVaild(LocalDateTime emailTokenVaild) {
        this.emailTokenVaild = emailTokenVaild;
    }

    public LocalDateTime getPasswordResetTokenVaild() {
        return passwordResetTokenVaild;
    }

    public void setPasswordResetTokenVaild(LocalDateTime passwordResetTokenVaild) {
        this.passwordResetTokenVaild = passwordResetTokenVaild;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    private LocalDateTime passwordResetTokenVaild;
    private boolean confirmed;

    public String getActiveToken() {
        return activeToken;
    }

    public void setActiveToken(String activeToken) {
        this.activeToken = activeToken;
    }

    private String activeToken;

    public LocalDateTime getTokenValidUntil() {
        return tokenValidUntil;
    }

    public void setTokenValidUntil(LocalDateTime tokenValidUntil) {
        this.tokenValidUntil = tokenValidUntil;
    }

    private LocalDateTime tokenValidUntil;


    public String getUrlWithToken(){
        return SsoUtils.getTokenUrl() + emailToken;
    }

    private boolean blocked;

}
