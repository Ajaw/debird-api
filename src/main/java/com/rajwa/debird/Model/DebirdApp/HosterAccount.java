package com.rajwa.debird.Model.DebirdApp;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by Dominik on 08.07.2018.
 */
@Document
public class HosterAccount {

    @Id
    private ObjectId id;
    private Hoster hoster;
    private Double transferLeft;
    private Date validUnitl;
    private String username;
    private String password;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public Hoster getHoster() {
        return hoster;
    }

    public void setHoster(Hoster hoster) {
        this.hoster = hoster;
    }

    public Double getTransferLeft() {
        return transferLeft;
    }

    public void setTransferLeft(Double transferLeft) {
        this.transferLeft = transferLeft;
    }

    public Date getValidUnitl() {
        return validUnitl;
    }

    public void setValidUnitl(Date validUnitl) {
        this.validUnitl = validUnitl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    private String queueName;

}
