package com.rajwa.debird.Model.DebirdApp;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by Dominik on 08.07.2018.
 */
@Document(collection="hoster")
public class Hoster {


    @Id
    private ObjectId id;

    public String getRealId() {
        return realId;
    }

    public void setRealId(String realId) {
        this.realId = realId;
    }

    private String realId;
    private List<HosterAccount> hosterAccountList;
    private String pathToImage;
    private boolean active;
    private boolean display;
    private Double totalTransferLeft;
    private String javaClassName;
    private String filePath;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public List<HosterAccount> getHosterAccountList() {
        return hosterAccountList;
    }

    public void setHosterAccountList(List<HosterAccount> hosterAccountList) {
        this.hosterAccountList = hosterAccountList;
    }

    public String getPathToImage() {
        return pathToImage;
    }

    public void setPathToImage(String pathToImage) {
        this.pathToImage = pathToImage;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isDisplay() {
        return display;
    }

    public void setDisplay(boolean display) {
        this.display = display;
    }

    public Double getTotalTransferLeft() {
        return totalTransferLeft;
    }

    public void setTotalTransferLeft(Double totalTransferLeft) {
        this.totalTransferLeft = totalTransferLeft;
    }

    public String getJavaClassName() {
        return javaClassName;
    }

    public void setJavaClassName(String javaClassName) {
        this.javaClassName = javaClassName;
    }

    public String getRegex() {
        return regex;
    }

    public void setRegex(String regex) {
        this.regex = regex;
    }

    private String regex;

}
