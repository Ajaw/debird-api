package com.rajwa.debird.Model.DTO;

import java.time.LocalDateTime;

/**
 * Created by Dominik on 26.08.2018.
 */
public class AuthDTO {

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public LocalDateTime getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(LocalDateTime validUntil) {
        this.validUntil = validUntil;
    }

    private LocalDateTime validUntil;

}
