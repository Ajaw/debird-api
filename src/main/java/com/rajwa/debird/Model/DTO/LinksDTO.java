package com.rajwa.debird.Model.DTO;

import java.util.List;

/**
 * Created by Dominik on 01.09.2018.
 */
public class LinksDTO {

    public List<LinkDTO> getLinkDTOList() {
        return linkDTOList;
    }

    public void setLinkDTOList(List<LinkDTO> linkDTOList) {
        this.linkDTOList = linkDTOList;
    }

    private List<LinkDTO> linkDTOList;

}
