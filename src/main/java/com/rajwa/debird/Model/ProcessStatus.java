package com.rajwa.debird.Model;

public class ProcessStatus  {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String id;
    private int precentComplete;
    private String message;

    public String getSsoToken() {
        return ssoToken;
    }

    public void setSsoToken(String ssoToken) {
        this.ssoToken = ssoToken;
    }

    private String ssoToken;

    public String getFileToken() {
        return fileToken;
    }

    public void setFileToken(String fileToken) {
        this.fileToken = fileToken;
    }

    private String fileToken;


    public int getPrecentComplete() {
        return precentComplete;
    }
    public void setPrecentComplete(int precentComplete) {
        this.precentComplete = precentComplete;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

}