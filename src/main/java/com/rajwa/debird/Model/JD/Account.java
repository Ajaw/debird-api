package com.rajwa.debird.Model.JD;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by Dominik on 08.07.2018.
 */
public class Account {

    private static final String VALID_UNTIL = "VALID_UNTIL";
    private static final String ACCOUNT_TYPE = "ACCOUNT_TYPE";
    private static final String LATEST_VALID_TIMESTAMP = "LATEST_VALID_TIMESTAMP";
    public static final String IS_MULTI_HOSTER_ACCOUNT = "IS_MULTI_HOSTER_ACCOUNT";
    private static final long serialVersionUID = -7578649066389032068L;

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    private boolean valid;

    public boolean isMulti() {
        return multi;
    }

    public void setMulti(boolean multi) {
        this.multi = multi;
    }

    private boolean multi;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isConcurrentUsePossible() {
        return concurrentUsePossible;
    }

    public void setConcurrentUsePossible(boolean concurrentUsePossible) {
        this.concurrentUsePossible = concurrentUsePossible;
    }

    private String user;
    private String pass;
    private boolean enabled = true;
    private boolean concurrentUsePossible = true;
    public static final String PROPERTY_TEMP_DISABLED_TIMEOUT = "PROPERTY_TEMP_DISABLED_TIMEOUT";
    public static final String PROPERTY_REFRESH_TIMEOUT = "PROPERTY_REFRESH_TIMEOUT";
    private static final String COOKIE_STORAGE = "COOKIE_STORAGE";
    private static final String BROWSER_COOKIES_STORAGE = "BROWSER_COOKIES_STORAGE";

    private Map<String, Object> accountPropertyMap = new LinkedHashMap<>();

    public boolean setProperty(String key, Object value) {
        if (IS_MULTI_HOSTER_ACCOUNT.equalsIgnoreCase(key)) {
            multi = value != null && Boolean.TRUE.equals(value);
        } else {
            if (AccountProperty.Property.NULL != value) {
                if ("nopremium".equalsIgnoreCase(key)) {
                    // convert.. some day we will use the setType only. The earlier we start to the correct fields, the better
                    if (Boolean.TRUE.equals(value)) {
                        setType(AccountType.FREE);
                    } else {
                        setType(AccountType.PREMIUM);
                    }
                } else if ("free".equalsIgnoreCase(key)) {
                    if (Boolean.TRUE.equals(value)) {
                        setType(AccountType.FREE);
                    } else {
                        setType(AccountType.PREMIUM);
                    }
                } else if ("session_type".equalsIgnoreCase(key)) {
                    if (!"premium".equals(value)) {
                        setType(AccountType.FREE);
                    } else {
                        setType(AccountType.PREMIUM);
                    }
                } else if ("premium".equalsIgnoreCase(key)) {
                    if (Boolean.TRUE.equals(value)) {
                        setType(AccountType.PREMIUM);
                    } else {
                        setType(AccountType.FREE);
                    }
                }
            }
        }
         accountPropertyMap.put(key,value);
        return true;
    }

    public void setType(AccountType type) {
        if (type == null) {
            setProperty(ACCOUNT_TYPE, AccountProperty.Property.NULL);
        } else {
            setProperty(ACCOUNT_TYPE, type.name());
        }
    }

    public static enum AccountType {
        FREE,
        PREMIUM,
        LIFETIME,
        UNKNOWN;
    }

    public String getStringProperty(final String key, final String def) {
        try {
            final Object r = getProperty(key, def);
            final String ret = (r == null) ? null : r.toString();
            return ret;
        } catch (final Exception e) {
            return def;
        }
    }

    public Object getProperty(final String key, final Object def) {
        Object ret = getProperty(key);
        if (def instanceof Long && ret instanceof Integer) {
            /* fix for integer in property map, but long wanted */
            ret = ((Integer) ret).longValue();
        }
        if (ret == null) {
            return def;
        }
        return ret;
    }


    public Object getProperty(final String key) {
        if (key == null) {

        }
        final Map<String, Object> lInternal = accountPropertyMap;
        if (lInternal == null || lInternal.size() == 0) {
            return null;
        } else {
                return lInternal.get(key);

        }
    }

    private Integer maxDownloads;

    public int getMaxSimultanDownloads() {
        return maxDownloads;
    }

    /**
     * -1 = unlimited, 0 = use deprecated getMaxSimultanPremiumDownloadNum/getMaxSimultanFreeDownloadNum,>1 = use this
     *
     * @since JD2
     */
    public void setMaxSimultanDownloads(int max) {
        if (max < 0) {
            maxDownloads = -1;
        } else {
            maxDownloads = max;
        }
    }

}
