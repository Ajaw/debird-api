package com.rajwa.debird.Model;

public class ProcessState {
    public static final int STATE_NOT_STARTED=0;
    public static final int STATE_BUSY=1;
    public static final int STATE_FINISHED=2;
}