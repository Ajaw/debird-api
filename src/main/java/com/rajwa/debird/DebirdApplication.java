package com.rajwa.debird;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.rajwa.debird.Config.StopSessionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandler;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;

import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Scanner;

@SpringBootApplication
@EnableScheduling
public class DebirdApplication {

	public static void main(String[] args) {
		SpringApplication.run(DebirdApplication.class, args);





	}

	@Bean
	public ThreadPoolTaskExecutor getAsyncExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(5);
		executor.setBeanName("threadPoolTaskExecutor");
		executor.setMaxPoolSize(50);
		executor.setQueueCapacity(10);
		executor.setThreadNamePrefix("MyExecutor-");
		executor.initialize();
		return executor;
	}

	@Autowired
	public SimpMessagingTemplate template;

	@Bean
	public WebSocketStompClient webSocketClient(){
		WebSocketClient webSocketClient = new StandardWebSocketClient();

		WebSocketStompClient socketStompClient = new WebSocketStompClient(webSocketClient);

		socketStompClient.setMessageConverter(new MappingJackson2MessageConverter());



		StompSessionHandler stompSessionHandler = new StopSessionHandler(template);
		String url ="ws://localhost:8084/newsocket";

		socketStompClient.connect(url,stompSessionHandler);


		return socketStompClient;
	}





}
